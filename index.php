<?php
include('connection.php');
$sql="select * from users";
$result=mysqli_query($db, $sql);
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <div class="header">
    </div>
  </div>
</head>
    <body>
      <ul>
        <li><a class="active"  href="#home" >Home</a></li>
        <li><a href="index.php" >Pick a Winner</a></li>
        <li><a href="#contact" >Winners</a></li>
        <li><a href="listofparticipants.php" >List of Participants</a></li>
      </ul>
        <button class="roll" onclick="rollClick()">Roll</button>
        <div class="names hide"></div>
        <div class="winner hide"></div>
        <a  class="roll-again hide" onclick="rollClick()">Roll Again?</a>
          
    <script>
        const ENTRANTS = ["Jan 10001", "Gilbert 10002", "Vincent 10003", "Owen 10004", "Angelo 10005", "Randy 10006", "Louis 10007", "Gabriel 10008", "Gerald 10009", "Stephen 10010", "John 10011", "Webert 10012", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "y", "z", "aa", "ab", "ac", "ad", "ae", "af", "ag", "ah", "ai", "aj", "ak", "al", "am", "an", "an", "ao", "ap", "aq", "ar", "as", "at", "au", "av", "ax", "ay", "az", "ba", "bc", "bd", "be", "bf", "bg", "bh", "bi", "bj", "bk", "bl", "bm", "bm", "bn", "bo", "bp", "bq", "br", "bs", "bt", "bu", "bv", "bx", "by", "bz", "ca", "cb", "cc", "cd", "ce", "cf", "cg", "ch", "ci", "cj", "ck", "cl", "cm", "cn", "co", "cp", "cq"];

const rollEl = document.querySelector(".roll");
const rollAgainEl = document.querySelector(".roll-again");
const namesEl = document.querySelector(".names");
const winnerEl = document.querySelector(".winner");

function randomName() {
  const rand = Math.floor(Math.random() * ENTRANTS.length);
  const name = ENTRANTS[rand];
  namesEl.innerText = name;
}

function rollClick() {
  rollEl.classList.add("hide");
  rollAgainEl.classList.add("hide");
  winnerEl.classList.add("hide");
  namesEl.classList.remove("hide");

  setDeceleratingTimeout(randomName, 10, 30);

  setTimeout(() => {
    namesEl.classList.add("hide");
    winnerEl.classList.remove("hide");
    rollAgainEl.classList.remove("hide");

    const winner = namesEl.innerText;
    winnerEl.innerText = winner;
    winnerEl.innerHTML = `<span>And the winner is...</span><br>${winner}`;
  }, 4000);
}

function setDeceleratingTimeout(callback, factor, times) {
  const internalCallback = ((t, counter) => {
    return () => {
      if (--t > 0) {
        setTimeout(internalCallback, ++counter * factor);
        callback();
      }
    };
  })(times, 0);

  setTimeout(internalCallback, factor);
}
    </script>
    
    
    
    </body>
</html>